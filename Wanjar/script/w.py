#! /usr/bin/python3

# A program to transcribe romanized classical wanjea into wanjea script

import os
from PIL import Image

list = list(input('Enter text to transcribe!\n'))

output = Image.new('RGBA', (11 * len(list), 12))
width = 0

for i in range(len(list)):
    image = Image.open(list[i] + '.png')
    output.paste(image, (width, 0))
    iwidth, iheight = image.size
    width += iwidth

cropoutput = output.crop((2, 0, width, 12))
cropoutput.save(input('Enter path/filename to save: ') + '.png', dpi=(72, 72))
